export const request = (options) => {
    console.log("RequestBuilder called for: " + options.url);
    //console.log(options);
    const headers = new Headers({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });
    //URL encoding
    // const BASE_URL = "http://localhost:8080";
    // let mapping = options.url.split(BASE_URL)[1];
    // //console.log("Mapping: " + mapping);
    // let routes = mapping.split("/");
    // options.url = BASE_URL;
    // //console.log(routes);
    // for (let i = 0; i < routes.length; i++) {
    //     if (routes[i] === "")
    //         continue;
    //     //console.log("route part: " + i);
    //     //console.log(encodeURIComponent(routes[i]));
    //     options.url += encodeURIComponent(routes[i]) + "/";
    // }
    //
    // //console.log("Encoded URL: " + options.url);
    // console.log("PRINT: " + encodeURI(options.url));

    // So the "#" in the url doesn't do problems
    console.log(options.url);
    if(options.url.indexOf('#') != -1){
        let params = options.url.split("&")[1];
        options.url = options.url.split("#")[0] + "&" + params;
    }

    console.log(options.url);
    //console.log("PRINT: " + options.url);
    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);


    return fetch(options.url, options)
        .then(response =>
            response.text().then(text => {
                if (!response.ok) {
                    return Promise.reject(text);
                }
                //console.log("Location AppApi:");
                //console.log(json.valueOf());
                return text;
            })
        ).catch(function (err) {
            console.log(err)
        });
};