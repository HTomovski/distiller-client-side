import React, {Component} from 'react';
import './App.css';
import {sendDistillingRequest} from "./repository/Fetch";

class App extends Component {
    constructor(props) {
        super(props);

        this.submitHandler = this.submitHandler.bind(this);
    }

    submitHandler = event => {
        event.preventDefault();
        let uri = document.getElementById("uriInput").value;
        if (uri === "") {
            window.alert("Please enter a valid url");
            return;
        }

        let OutputFormat;
        for (let format of document.getElementsByName("returnFormat")) {
            //console.log(format);
            if (format.checked) {
                OutputFormat = format.id;
            }
        }

        let outputFormat = "outputFormat=" + OutputFormat;

        let uriInput = "url=" + document.getElementById("uriInput").value + "&";

        sendDistillingRequest(uriInput, outputFormat).then(response => {
            console.log(response);
            document.getElementById("result").innerText = ""+response;
        });
    };
npm
    render() {
        return (
            <div>
                <div className={"container"}>
                    <form onSubmit={this.submitHandler}>
                        <div className={"form-group"}>
                            <label>
                                <h2>Give us a URI and we will extract any structured RDF from it!</h2>
                                <input id={"uriInput"}
                                       type={"text"}
                                       placeholder={"Paste your URI here"}
                                       className={"form-control"}/>
                            </label>
                        </div>

                        <div className={"form-group"}>
                            <h4>What format do you want the extracted RDF in?</h4>
                            <label><input type={"radio"} id={"radRdfXml"} name={"returnFormat"}
                                          className={"form-control"} defaultChecked={true}/> RDF/XML</label>
                            <label><input type={"radio"} id={"radTurtle"} name={"returnFormat"}
                                          className={"form-control"}/> Turtle</label>
                            <label><input type={"radio"} id={"radNTriples"} name={"returnFormat"}
                                          className={"form-control"}/> N Triples</label>
                            <label><input type={"radio"} id={"radJsonLd"} name={"returnFormat"}
                                          className={"form-control"}/> JSON-LD</label>
                        </div>

                        <div className="form-group">
                            <input type="submit"
                                   name="extract"
                                   id="extractButton"
                                   className="form-control form-submit"
                                   value="Extract!"/>
                        </div>
                    </form>

                    <div id={"result"}>

                    </div>
                </div>
            </div>
        );
    }
}

export default App;
