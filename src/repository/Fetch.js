import { request } from "../utils/RequestBuilder";

const BASE_URL = 'http://localhost:8080';

export const sendDistillingRequest = (uriInput, outputFormat) => {
    //console.log(BASE_URL + "?" + inputFormats + outputFormat);
    return request({
        url: BASE_URL + "?" + uriInput + outputFormat,
        method: 'GET'
    });
};


